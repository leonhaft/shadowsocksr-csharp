﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shadowsocks.Model
{
    [Serializable]
    public class FreeProvider
    {
        public string Url { get; set; }
        public string GroupName { get; set; }
        public long LastUpdate { get; set; }

        public FindServerMode Mode { get; set; }

        public UpdateMethod Update { get; set; }

        public FreeProvider()
        {
            Url = "Free.com";
            GroupName = "Free";
            Mode = FindServerMode.Text;
            Update = UpdateMethod.ByPassProxy;
        }
    }

    public enum FindServerMode
    {
        /// <summary>
        /// 查找网页中的二维码图片
        /// </summary>
        QRCode,
        /// <summary>
        /// 查找网页中的SSR或SS地址
        /// </summary>
        Text
    }

    public enum UpdateMethod
    {
        /// <summary>
        /// 不通过代理更新
        /// </summary>
        ByPassProxy,
        /// <summary>
        /// 通过代理更新
        /// </summary>
        ByProxy
    }
}
