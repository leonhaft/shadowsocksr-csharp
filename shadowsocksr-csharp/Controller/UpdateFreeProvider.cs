﻿using Shadowsocks.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace Shadowsocks.Controller
{
    public class UpdateFreeProvider
    {
        private Configuration configuration;
        private ShadowsocksController shadowsocksController;
        private static readonly string linkHttpRegexPattern = @"<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)</a>";
        private static readonly string HRefPattern = "href\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
        private static readonly string imageEndPattern = "(jpeg|jpg|png|gif|bmp)$";
        public static readonly Regex UrlFinder = new Regex(@"(ss|ssr)://(?<base64>[A-Za-z0-9+-/=_]+)(?:#(?<tag>\S+))?", RegexOptions.IgnoreCase);
        public static readonly Regex ssUrlFinder = new Regex(@"ss://(?<base64>[A-Za-z0-9+-/=_]+)(?:#(?<tag>\S+))?", RegexOptions.IgnoreCase);
        public static readonly Regex ssrUrlFinder = new Regex(@"ssr://(?<base64>[A-Za-z0-9+-/=_]+)(?:#(?<tag>\S+))?", RegexOptions.IgnoreCase);
        public static readonly Regex DetailsParser = new Regex(@"^((?<method>.+?):(?<password>.*)@(?<hostname>.+?):(?<port>\d+?))$", RegexOptions.IgnoreCase);

        public UpdateFreeProvider(ShadowsocksController controller)
        {
            shadowsocksController = controller;
            this.configuration = controller.GetConfiguration();
        }
        public void Update()
        {
            if (configuration.freeProviders == null || configuration.freeProviders.Count == 0)
            {
                return;
            }
            foreach (var provider in configuration.freeProviders.OrderBy(s => s.Update))
            {
                UpdateProvider(provider);
            }

        }

        private void UpdateProvider(FreeProvider provider)
        {
            var newServers = new List<Server>();
            try
            {
                var httpSource = HttpHelper.GetString(configuration, provider);
                if (httpSource == null)
                {
                    return;
                }
                if (provider.Mode == FindServerMode.QRCode)
                {
                    var findedServers = FindQRCode(httpSource, provider);
                    if (findedServers != null)
                    {
                        newServers.AddRange(findedServers);
                    }
                }
                else if (provider.Mode == FindServerMode.Text)
                {
                    var findedServers = FindUrl(httpSource, provider);
                    if (findedServers != null)
                    {
                        newServers.AddRange(findedServers);
                    }
                }
                MegreServer(newServers);
                provider.LastUpdate = DateTime.Now.Ticks;
                Configuration.Save(this.configuration);
                shadowsocksController.SaveServersConfig(this.configuration);
            }
            catch (Exception ex)
            {
                Logging.LogUsefulException(ex);
            }
        }

        private void MegreServer(List<Server> newServers)
        {
            foreach (var server in newServers)
            {
                var findOldServer = configuration.configs.FirstOrDefault(s => s.server == server.server && s.obfs == server.obfs);
                if (findOldServer != null)
                {
                    configuration.configs.Remove(findOldServer);
                }
                configuration.configs.Add(server);
            }
        }
        private List<Server> FindQRCode(string pageSource, FreeProvider provider)
        {
            var imageUrls = FindQRCodeUrl(pageSource);
            var servers = new List<Server>();

            foreach (var url in imageUrls)
            {
                var stream = HttpHelper.GetStream(configuration, provider, new Uri(new Uri(provider.Url), url));
                if (stream != null)
                {
                    var findedUrls = DecodeQRCode(stream);
                    if (findedUrls == null || findedUrls.Any() == false)
                    {
                        continue;
                    }
                    foreach (var fu in findedUrls)
                    {
                        var server = new Server(fu, provider.GroupName);
                        servers.Add(server);
                    }
                }
            }
            return servers;
        }

        private List<string> FindQRCodeUrl(string pageSource)
        {
            var matchLink = new List<string>();


            var match = Regex.Match(pageSource, HRefPattern,
                              RegexOptions.IgnoreCase | RegexOptions.Compiled,
                              TimeSpan.FromSeconds(1));
            while (match.Success)
            {
                var matchValue = match.Groups[1].Value;
                if (IsImageExtensionEnd(matchValue))
                {
                    matchLink.Add(matchValue);
                }
                match = match.NextMatch();
            }
            return matchLink;
        }

        private bool IsImageExtensionEnd(string href)
        {
            if (href == null)
                return false;
            return Regex.IsMatch(href, imageEndPattern);
        }
        private List<Server> FindUrl(string pageSource, FreeProvider provider)
        {
            try
            {
                var urls = FindSSOrSSRAddress(pageSource);
                var servers = FindServer(urls, provider.GroupName);
                return servers;
            }
            catch (Exception ex)
            {
                Logging.LogUsefulException(ex);
                return null;
            }
        }

        private List<string> FindSSOrSSRAddress(string pageSource)
        {
            var serverUrls = new List<string>();
            var ssMatchs = UrlFinder.Match(pageSource);
            while (ssMatchs.Success)
            {
                if (serverUrls.Any(s => s == ssMatchs.Value) == false)
                {
                    serverUrls.Add(ssMatchs.Value);
                }
                ssMatchs = ssMatchs.NextMatch();
            }

            return serverUrls;
        }

        private List<Server> FindServer(List<string> urls, string groupName)
        {
            var newServers = new List<Server>();

            foreach (var url in urls.OrderByDescending(s => s))
            {
                try
                {
                    var server = new Server(url, groupName);
                    if (newServers.Any(s => s.server == server.server) == false)
                    {
                        newServers.Add(server);
                    }
                }
                catch (System.FormatException e)
                {

                }

            }

            return newServers;
        }
        public bool ServerFromSSR(string ssrURL)
        {
            // ssr://host:port:protocol:method:obfs:base64pass/?obfsparam=base64&remarks=base64&group=base64&udpport=0&uot=1
            try
            {
                Match ssr = Regex.Match(ssrURL, "ssr://([A-Za-z0-9_-]+)", RegexOptions.IgnoreCase);
                if (!ssr.Success)
                    return false;

                string data = Util.Base64.DecodeUrlSafeBase64(ssr.Groups[1].Value);
                Dictionary<string, string> params_dict = new Dictionary<string, string>();

                Match match = null;
                for (int nTry = 0; nTry < 2; ++nTry)
                {
                    int param_start_pos = data.IndexOf("?");
                    if (param_start_pos > 0)
                    {
                        params_dict = ParseParam(data.Substring(param_start_pos + 1));
                        data = data.Substring(0, param_start_pos);
                    }
                    if (data.IndexOf("/") >= 0)
                    {
                        data = data.Substring(0, data.LastIndexOf("/"));
                    }

                    Regex UrlFinder = new Regex("^(.+):([^:]+):([^:]*):([^:]+):([^:]*):([^:]+)");
                    match = UrlFinder.Match(data);
                    if (match.Success)
                        break;
                    ssr = Regex.Match(ssrURL, @"ssr://([A-Za-z0-9-_.:=?&/\[\]]+)", RegexOptions.IgnoreCase);
                    if (ssr.Success)
                        data = ssr.Groups[1].Value;
                    else
                        return false;
                }
                if (match == null || !match.Success)
                    return false;

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        private List<Server> FindSSUrlAndServers(string urlSource)
        {
            var serverUrls = new List<Server>();
            var ssMatchs = ssUrlFinder.Match(urlSource);
            while (ssMatchs.Success)
            {
                serverUrls.AddRange(GetServers(ssMatchs.Value));
                ssMatchs = ssMatchs.NextMatch();
            }
            return serverUrls;
        }
        public List<Server> GetServers(string ssURL)
        {
            var matches = ssUrlFinder.Matches(ssURL);
            if (matches.Count <= 0) return null;
            List<Server> servers = new List<Server>();
            foreach (Match match in matches)
            {
                Server tmp = new Server();
                var base64 = match.Groups["base64"].Value;
                var tag = match.Groups["tag"].Value;
                if (string.IsNullOrEmpty(tag) == false)
                {
                    tmp.remarks = HttpUtility.UrlDecode(tag, Encoding.UTF8);
                }
                Match details = DetailsParser.Match(Encoding.UTF8.GetString(Convert.FromBase64String(
                    base64.PadRight(base64.Length + (4 - base64.Length % 4) % 4, '='))));
                if (!details.Success)
                    continue;
                tmp.method = details.Groups["method"].Value;
                tmp.password = details.Groups["password"].Value;
                tmp.server = details.Groups["hostname"].Value;
                tmp.server_port = int.Parse(details.Groups["port"].Value);

                servers.Add(tmp);
            }
            return servers;
        }

        public List<string> DecodeQRCode(Stream qrCodeStream)
        {
            try
            {
                var reader = new QRCodeReader();
                var providerFreeServerUrls = new List<string>();

                var bitmap = (Bitmap)Bitmap.FromStream(qrCodeStream);
                var source = new BitmapLuminanceSource(bitmap);
                var binaryBitMap = new BinaryBitmap(new HybridBinarizer(source));
                var result = reader.decode(binaryBitMap);
                if (result != null)
                {
                    providerFreeServerUrls.Add(result.Text);
                }
                return providerFreeServerUrls;
            }
            catch
            {
                return null;
            }

        }

        private Dictionary<string, string> ParseParam(string param_str)
        {
            Dictionary<string, string> params_dict = new Dictionary<string, string>();
            string[] obfs_params = param_str.Split('&');
            foreach (string p in obfs_params)
            {
                if (p.IndexOf('=') > 0)
                {
                    int index = p.IndexOf('=');
                    string key, val;
                    key = p.Substring(0, index);
                    val = p.Substring(index + 1);
                    params_dict[key] = val;
                }
            }
            return params_dict;
        }
    }

    public class HttpHelper
    {
        public static string GetString(Configuration configuration, FreeProvider freeProvider)
        {
            HttpClient client = null;
            var useProxy = freeProvider.Update == UpdateMethod.ByProxy;
            if (useProxy)
            {
                HttpClientHandler handler = new HttpClientHandler
                {
                    UseProxy = useProxy,
                    Proxy = new WebProxy(IPAddress.Loopback.ToString(), configuration.localPort)
                };

                if (String.IsNullOrEmpty(configuration.authPass) == false)
                {
                    handler.Proxy.Credentials = new NetworkCredential(configuration.authUser, configuration.authPass);
                }
                client = new HttpClient(handler, true);

            }
            else
            {
                client = new HttpClient();
            }


            try
            {
                var response = client.GetStringAsync(freeProvider.Url);
                response.Wait();
                return response.Result;
            }
            catch (Exception ex)
            {
                Logging.Error($"download web source string fail:{freeProvider.Url.ToString()}");
                return null;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static Stream GetStream(Configuration configuration, FreeProvider freeProvider, Uri streamUrl)
        {
            HttpClient client = null;
            var useProxy = freeProvider.Update == UpdateMethod.ByProxy;
            if (useProxy)
            {
                HttpClientHandler handler = new HttpClientHandler
                {
                    UseProxy = freeProvider.Update == UpdateMethod.ByProxy,
                    Proxy = new WebProxy(IPAddress.Loopback.ToString(), configuration.localPort)
                };

                if (String.IsNullOrEmpty(configuration.authPass) == false)
                {
                    handler.Proxy.Credentials = new NetworkCredential(configuration.authUser, configuration.authPass);
                }
                client = new HttpClient(handler, true);

            }
            else
            {
                client = new HttpClient();
            }

            try
            {
                var response = client.GetStreamAsync(streamUrl);
                response.Wait();
                return response.Result;
            }
            catch (Exception ex)
            {
                Logging.Error($"download QRCode stream fail:{streamUrl.ToString()}");
                return null;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}
