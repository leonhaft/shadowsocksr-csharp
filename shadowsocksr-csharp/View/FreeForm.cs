﻿using Shadowsocks.Controller;
using Shadowsocks.Model;
using Shadowsocks.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shadowsocks.View
{
    public partial class FreeForm : Form
    {
        private ShadowsocksController controller;
        private Configuration currentModifiedConfiguration;
        private int currentSelectIndex;

        public FreeForm(ShadowsocksController controller)
        {
            this.Font = System.Drawing.SystemFonts.MessageBoxFont;
            InitializeComponent();

            this.Icon = Icon.FromHandle(Resources.ssw128.GetHicon());
            this.controller = controller;
            currentModifiedConfiguration = controller.GetConfiguration();
            UpdateTexts();
            LbxFreeServers.SelectedIndexChanged += LbxFreeServers_SelectedIndexChanged;
            //controller.ConfigChanged += controller_ConfigChanged;
            BindMethod();
            LoadCurrentConfiguration();
        }

        private void LbxFreeServers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectIndex = LbxFreeServers.SelectedIndex;
            currentSelectIndex = LbxFreeServers.SelectedIndex;
            if (currentSelectIndex == -1 || currentSelectIndex >= LbxFreeServers.Items.Count)
            {
                BtnAdd.Enabled = false;
                return;
            }
            var currentSelectProvider = currentModifiedConfiguration.freeProviders[selectIndex];
            TxtUrl.Text = currentSelectProvider.Url;
            TxtGroupName.Text = currentSelectProvider.GroupName;
            var methodSelectIndex = SelectedMode(currentSelectProvider.Mode);
            CbxMethod.SelectedIndex = methodSelectIndex;
            CheckUpdateMethod(currentSelectProvider);
            if (currentSelectProvider.LastUpdate != 0)
            {
                TxtLastUpdated.Text = ConvertToDateTime(currentSelectProvider.LastUpdate);
            }
            else
            {
                TxtLastUpdated.Text = "";
            }
        }

        private int SelectedMode(FindServerMode methond)
        {
            var findedIndex = -1;
            for (var itemIndex = 0; itemIndex < CbxMethod.Items.Count; itemIndex++)
            {
                if (string.Compare(CbxMethod.Items[itemIndex].ToString(), Enum.GetName(typeof(FindServerMode), methond), true) == 0)
                {
                    findedIndex = itemIndex;
                    break;
                }
            }
            return findedIndex;
        }

        private void UpdateTexts()
        {
            this.Text = I18N.GetString("Free Subscribe");
            label1.Text = I18N.GetString("URL");
            label2.Text = I18N.GetString("Group name");
            label4.Text = I18N.GetString("Find Mode");
            BtnOk.Text = I18N.GetString("OK");
            BtnCancel.Text = I18N.GetString("Cancel");
            label3.Text = I18N.GetString("Last Update");
            BtnAdd.Text = I18N.GetString("&Add");
            BtnDelete.Text = I18N.GetString("&Delete");
            RbByProxy.Text = I18N.GetString("Update By Proxy");
            RbByPassProxy.Text = I18N.GetString("Update Bypass proxy");
            label5.Text = I18N.GetString("Update Method");
            //BtnUpdate.Text = I18N.GetString("Update Free");
            //BtnUpdateByPass.Text = I18N.GetString("Update Free(Bypass proxy)");
        }

        private void BindMethod()
        {
            var names = Enum.GetNames(typeof(FindServerMode));
            foreach (var name in names)
            {
                CbxMethod.Items.Add(name);
            }
        }

        private string ConvertToDateTime(long ticks)
        {
            DateTime now = new DateTime(ticks);
            return now.ToLongDateString() + " " + now.ToLongTimeString();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtUrl.Text) || string.IsNullOrEmpty(TxtGroupName.Text) || CbxMethod.SelectedIndex == -1)
            {
                return;
            }
            var currentProvider = currentModifiedConfiguration.freeProviders[currentSelectIndex];
            currentProvider.Url = TxtUrl.Text;
            currentProvider.GroupName = TxtGroupName.Text;
            currentProvider.Mode = (FindServerMode)Enum.Parse(typeof(FindServerMode), CbxMethod.SelectedItem.ToString());
            currentProvider.Update = RbByPassProxy.Checked ? UpdateMethod.ByPassProxy : UpdateMethod.ByProxy;
            SetSelected(currentSelectIndex);
            controller.SaveServersConfig(currentModifiedConfiguration);
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadCurrentConfiguration()
        {
            currentModifiedConfiguration = controller.GetConfiguration();
            LoadAllSettings();
            if (LbxFreeServers.Items.Count == 0)
            {
                BtnOk.Enabled = false;
            }
            else
            {
                BtnOk.Enabled = true;
            }
        }

        private void SetSelected(int index)
        {
            if (index >= 0 && index < currentModifiedConfiguration.freeProviders.Count)
            {
                var provider = currentModifiedConfiguration.freeProviders[index];
                if (provider.Url != TxtUrl.Text)
                {
                    provider.Url = TxtUrl.Text;
                    provider.GroupName = "";
                    CbxMethod.SelectedIndex = SelectedMode(provider.Mode);
                    CheckUpdateMethod(provider);
                    provider.LastUpdate = 0;
                }
            }
        }

        private void CheckUpdateMethod(FreeProvider provider)
        {
            if (provider.Update == UpdateMethod.ByPassProxy)
            {
                RbByPassProxy.Checked = true;
            }
            else if (provider.Update == UpdateMethod.ByProxy)
            {
                RbByProxy.Checked = true;
            }
        }

        private void SetContent(FreeProvider provider)
        {
            TxtUrl.Text = provider.Url;
            TxtGroupName.Text = provider.GroupName;
            CbxMethod.SelectedIndex = SelectedMode(provider.Mode);
            TxtLastUpdated.Text = ConvertToDateTime(provider.LastUpdate);
        }

        private void Clear()
        {
            TxtUrl.Text = "";
            TxtGroupName.Text = "";
            TxtLastUpdated.Text = "";
        }

        private void UpdateSelected(int index)
        {
            if (index >= 0 && index < currentModifiedConfiguration.freeProviders.Count)
            {
                var provider = currentModifiedConfiguration.freeProviders[index];
                if (provider.Url != TxtUrl.Text)
                {
                    SetContent(provider);
                }
            }
        }

        private void LoadAllSettings()
        {
            BindingSettings();
        }
        private void BindingSettings()
        {
            LbxFreeServers.Items.Clear();
            foreach (var provider in currentModifiedConfiguration.freeProviders)
            {
                LbxFreeServers.Items.Add(FormatListView(provider));
            }
        }

        private string FormatListView(FreeProvider provider)
        {
            return (String.IsNullOrEmpty(provider.GroupName) ? " " : provider.GroupName + " - ") + provider.Url;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            var provider = new FreeProvider();
            currentModifiedConfiguration.freeProviders.Add(provider);
            BindingSettings();
            currentSelectIndex = currentModifiedConfiguration.freeProviders.Count - 1;
            LbxFreeServers.SelectedIndex = currentSelectIndex;
            BtnOk.Enabled = true;
            //BtnUpdate.Enabled = false;
            //BtnUpdateByPass.Enabled = false;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            var deleteIndex = LbxFreeServers.SelectedIndex;
            if (deleteIndex != -1 && deleteIndex < LbxFreeServers.Items.Count)
            {
                currentModifiedConfiguration.freeProviders.RemoveAt(deleteIndex);
                BindingSettings();
                if (deleteIndex == 0)
                {
                    currentSelectIndex = deleteIndex + 1;
                }
                else
                {
                    currentSelectIndex = deleteIndex - 1;
                }

                if (LbxFreeServers.Items.Count != 0)
                {
                    LbxFreeServers.SelectedIndex = currentSelectIndex;
                }
                else
                {
                    Clear();
                    SetSelected(currentSelectIndex);
                }
                //UpdateSelected(currentSelectIndex);
            }
        }

        //private void BtnUpdate_Click(object sender, EventArgs e)
        //{
        //    if (CheckBeforeUpdate())
        //    {
        //        return;
        //    }
        //    var s = new UpdateFreeProvider(currentModifiedConfiguration);
        //    var provider = currentModifiedConfiguration.freeProviders[currentSelectIndex];
        //    s.Update(provider, true);
        //}

        //private void BtnUpdateByPass_Click(object sender, EventArgs e)
        //{
        //    if (CheckBeforeUpdate())
        //    {
        //        return;
        //    }
        //    var s = new UpdateFreeProvider(currentModifiedConfiguration);
        //    var provider = currentModifiedConfiguration.freeProviders[currentSelectIndex];

        //    s.Update(provider, false);
        //}

        private bool CheckBeforeUpdate()
        {
            if (currentSelectIndex == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RbByPassProxy_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                RbByProxy.Checked = false;
            }
        }

        private void RbByProxy_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                RbByPassProxy.Checked = false;
            }
        }
    }
}
